$(document).ready(function () {
  if ($(this).scrollTop() > 1) {
    $(".page__header").addClass("fixed");
  } else {
    $(".page__header").removeClass("fixed");
  }
  $(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
      $(".page__header").addClass("fixed");
    } else {
      $(".page__header").removeClass("fixed");
    }
  });

  // красивый select
  if ($(".js-select").length) {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
      // closeOnSelect: false,
    });
  }
  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  $("input").on("change", function () {
    if ($(this).val().length) {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
  // /маска для инпупов
  $(window).resize(function () {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
    });
  });

  $(".js-header-search-toggle").on("click", function () {
    $(".js-header-search-cross").toggleClass("open");
    $(".js-header-search-form").toggleClass("open");
    $(".header-search__results").removeClass("open");
  });

  $(".js-header-search-input").on("input", function () {
    $(".header-search__results").addClass("open");
  });

  $(".js-mobile-menu-title").on("click", function () {
    if (!$(this).hasClass("open")) {
      $(".mobile-menu__fulllink.open").removeClass("open");
      $(".js-mobile-menu-title.open").removeClass("open");
      $(".js-mobile-menu-menu").hide();
    }

    $(this).toggleClass("open");
    $(this)
      .parents(".js-mobile-menu-block")
      .find(".js-mobile-menu-menu")
      .slideToggle();
    $(this)
      .parents(".js-mobile-menu-block")
      .find(".mobile-menu__submenu")
      .hide();
  });
  $(".js-mobile-menu-arr").on("click", function () {
    if (
      !$(this)
        .parents(".js-mobile-menu-block")
        .find(".mobile-menu__fulllink")
        .hasClass("open")
    ) {
      $(".mobile-menu__fulllink.open").removeClass("open");
      $(".mobile-menu__submenu").hide();
    }

    $(this)
      .parents(".js-mobile-menu-block")
      .find(".mobile-menu__fulllink")
      .toggleClass("open");
    $(this)
      .parents(".js-mobile-menu-block")
      .find(".mobile-menu__submenu")
      .slideToggle();
  });

  $(".js-mobile-menu__opener").on("click", function () {
    $("body").toggleClass("fixed");
    $(this).toggleClass("open");
    $(".js-mobile-menu__content").slideToggle();
  });
  // карты
  ymaps.ready(function () {
    init("map");
    init("map-mobile");
  });

  function init(newMap) {
    var myMap = new ymaps.Map(
      newMap,
      {
        center: [55.728053, 37.567276],
        zoom: 16,
        controls: ["fullscreenControl"],
      },
      { suppressMapOpenBlock: true }
    );

    myMap.geoObjects.add(
      new ymaps.Placemark(
        myMap.getCenter(),
        {
          balloonContent: "Рщсконсалт",
        },
        {
          preset: "islands#icon",
          iconColor: "#CC0001",
        }
      )
    );
  }
  // карты

  $(".js-quiz__btn--next").on("click", function (e) {
    e.preventDefault();
    let active = $(".quiz__step.active");
    let next = active.next(".quiz__step");
    active.removeClass("active").hide();
    active.next(".quiz__step").addClass("active").show();
    if (!next.next(".quiz__step").length) {
      $(".js-quiz__btn--next").hide();
      $(".js-quiz__btn--btn").show();
    }
    $(".js-quiz__btn--prev").removeClass("disabled");
    let step = parseInt($(".quiz__active").html());
    $(".quiz__active").html(++step);
  });
  $(".js-quiz__btn--prev").on("click", function (e) {
    e.preventDefault();
    let active = $(".quiz__step.active");
    let prev = active.prev(".quiz__step");
    active.removeClass("active").hide();
    active.prev(".quiz__step").addClass("active").show();

    if (!prev.prev(".quiz__step").length) {
      $(".js-quiz__btn--prev").addClass("disabled");
    }
    $(".js-quiz__btn--next").show();
    $(".js-quiz__btn--btn").hide();
    let step = parseInt($(".quiz__active").html());
    $(".quiz__active").html(--step);
  });

  if ($(".js-services").length) {
    if ($(window).width() > 767) {
      let count = ($('.js-services').innerWidth() / $('.services__slide').innerWidth()).toFixed()
      $('.js-services-active').text(1 +'-'+ (1 + parseInt(count)))

      var swiper = new Swiper(".js-services", {
        slidesPerView: 'auto',
        navigation: {
          nextEl: ".js-services-next",
          prevEl: ".js-services-prev",
        },
      });

      swiper.on('transitionEnd', function() {
        console.log(swiper)
        let count = ($('.js-services').innerWidth() / $('.services__slide').innerWidth()).toFixed()
        $('.js-services-active').text(parseInt(swiper.activeIndex) + 1 +'-'+ (parseInt(swiper.activeIndex) + 1 + parseInt(count)));
        let img = $('.services__slide:nth-child(' + (parseInt(swiper.activeIndex) + 1) + ') img').attr('src');

      $('.js-services ').css('background-image', 'url('+ img + ')')

      });
    } 
    let img = $('.services__slide:first-child img').attr('src')
    $('.js-services ').css('background-image', 'url('+ img + ')')

    $('.services__slide').on('mouseenter mouseleave', function() {
      let img = $(this).find('.services__img').attr('src')
      $('.js-services ').css('background-image', 'url('+ img + ')')
    })
  }

  $('.js-sro-tab').on('click', function(e) {
    e.preventDefault()
    let href=$(this).attr('href')


    $('.js-sro-tab.active').removeClass('active')
    $('.sro__content.active').removeClass('active')
    $(this).addClass('active')
    $(href).addClass('active')

  })

  if ($(".js-reviews").length) {
      var swiper = new Swiper(".js-reviews", {
        grabCursor: true,
        effect: "creative",
        navigation: {
          nextEl: ".js-reviews-next",
          prevEl: ".js-reviews-prev",
        },
        creativeEffect: {
          prev: {
            translate: ["-40%", 0, -200],
            opacity: 0
          },
          next: {
            translate: ["40%", 0, -200],
          },
        },
      });
    }

});
